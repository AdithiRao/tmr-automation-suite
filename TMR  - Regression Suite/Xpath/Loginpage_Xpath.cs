﻿using OpenQA.Selenium;

namespace TMR_RegressionSuite
{
    public static class Loginpage_Xpath
    {
        public static By Username => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_Login1_UserName"); 
        public static By Password => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_Login1_Password");
        public static By Sign_In => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_Login1_LoginButton"); 
        public static By Header_Site_name => By.Id("header-sitename");
        public static By Header_Logo => By.XPath("//img[@src='/TMR/Content/Images/qg-logo-colour.png']");
        public static By Header_Text => By.XPath("//img[@alt='Queensland Government']");
        public static By Login_Failure_Text => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_Login1_FailureText");
        public static By Forgotten_Password => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_lnkPasswdRetrieval");
        public static By Homepage_Email => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_txtPasswordRetrieval");
        public static By Reset_Password => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl01_LogonForm_btnPasswdRetrieval");
    }
}