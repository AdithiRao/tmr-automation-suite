﻿using OpenQA.Selenium;

namespace TMR_RegressionSuite.Xpath
{
    public static class CreateEvent_Xpath
    {
        public static By Create_Link => By.XPath("//a[@href ='/CreateEvent']");
        public static By Create_Event_Tab => By.XPath("//a[@href ='/CreateEvent']");

        // *** WHAT Section ***
        public static By Create_Event_What_Header_text1 => By.XPath("//span[contains(@class,'panel-title')][contains(text(),'What?')]");
        public static By Create_Event_What_Header_EventType_text2 => By.XPath("//span[contains(@class,'panel-title-description')][contains(text(),'Event Type')]");
        public static By Create_Event_What_EventType_Dropdown => By.Id("eventTypeSelect");
        public static By Create_Event_What_EventType_Dropdown_Label => By.XPath("//label[contains(@class,'col-sm-4 control-label')][contains(text(),'Event Type')]");
        public static By Create_Event_What_SubType_Dropdown => By.XPath("//select[contains(@id,'eventSubtypeSelect')]");
        public static By Create_Event_What_SubType_Dropdown_Label => By.XPath("//label[contains(@class,'col-sm-4 control-label')][contains(text(),'Sub-Type')]");
        public static By Create_Event_What_Web_Link => By.XPath("//label[contains(@class,'col-sm-4 control-label')][contains(text(),'Web Link')]");
        public static By Create_Event_What_Web_Link_Textbox => By.Id("eventWebLink");
        public static By Create_Event_What_Web_Link_Textbox_PlaceHolder => By.XPath("//input[contains(@placeholder,'< enter weblink if applicable >')]");
        public static By Create_Event_What_Image => By.XPath("//div[contains(@class,'form-group')][5]//label[contains(text(),'Image')]");
        public static By Create_Event_What_Attachment_Icon => By.XPath("//button[contains(@title,'Attachments')]");
        public static By Create_Event_What_Done_button => By.XPath("//button[contains(@class,'btn btn-primary pull-right')][contains(text(),'Done')]");

        // *** WHERE Section ***
        public static By Create_Event_Where_Header_text1 => By.XPath("//span[contains(@class,'panel-title')][contains(text(),'Where?')]");
        public static By Create_Event_Where_Header_EventLocation_text2 => By.XPath("//span[contains(@class,'panel-title-description')][contains(text(),'Event Location')]");
        public static By Create_Event_Where_Origin_Text_Box => By.Id("mapStartingPoint");
        public static By Create_Event_Where_Origin_Placeholder_Text => By.XPath("//input[@placeholder='Origin']");
        public static By Create_Event_Where_Destination_Text_Box => By.Id("mapEndingPoint");
        public static By Create_Event_Where_Destination_Placeholder_Text => By.XPath("//input[@placeholder='Destination']");
        public static By Create_Event_Where_AddLocation_Button => By.Id("AddLocationButton");

        //Origin Panel - to be revisted after Carly's fix
        public static By Create_Event_Where_Origin_Panel_Title => By.XPath("//div[@class='panel-title'][contains(text(),'Origin')]");
        public static By Create_Event_Where_Origin_Panel_Address_Label => By.XPath("//div[@class='col-md-3 col-lg-2']//strong[contains(text(),'Address')]");
        public static By Create_Event_Where_Origin_Panel_Address => By.XPath("//span[@data-bind='text: $data.place().formatted_address']");
        public static By Create_Event_Where_Origin_Panel_Alternate_Address => By.Id("AltAddressOrigin");
        public static By Create_Event_Where_Origin_Panel_LGA_Label => By.XPath("//div[@class='col-md-3 col-lg-2']//strong[contains(text(),'LGA')]");
        public static By Create_Event_Where_Origin_Panel_LGA => By.XPath("//span[@data-bind='text: $data.point().localGovernmentArea.name']");
        public static By Create_Event_Where_Origin_Panel_Road_ID_Label => By.XPath("//div[@class='col-md-12 col-lg-7']//strong[contains(text(),'Road Id')]");
        public static By Create_Event_Where_Origin_Panel_Road_ID => By.XPath("");
        public static By Create_Event_Where_Origin_Panel_District_Label => By.XPath("//div[@class='col-md-3 col-lg-2']//strong[contains(text(),'District')]");
        public static By Create_Event_Where_Origin_Panel_District => By.XPath("//span[@data-bind='text: $data.point().district.name']");
        public static By Create_Event_Where_Origin_Panel_TDist_Label => By.XPath("//div[@class='col-md-3 col-lg-2']//strong[contains(text(),'T-Dist')]");
        public static By Create_Event_Where_Origin_Panel_TDist => By.XPath("");

        //Destination Panel - to be revisted after Carly's fix
        public static By Create_Event_Where_Destination_Panel_Title => By.XPath("//div[@class='panel-title'][contains(text(),'Destination')]");

        //Location Suggestion 
        public static By Create_Event_Where_Location_Number_Heading => By.XPath("//div[contains(@class,'locationNumberHeading')]");
        public static By Create_Event_Where_Location_Remove_Location => By.XPath("//button[contains(@class,'btn btn-sm btn-primary pull-right')][contains(@title,'Remove Location')]");
        public static By Create_Event_Where_Location_Edit_Location => By.XPath("//button[contains(@class,'btn btn-sm btn-primary pull-right')][contains(@title,'Edit Location')]");
        public static By Create_Event_Where_Location_Primary_Road_Name_Label => By.XPath("//label[contains(text(),'Primary Road Name')]");
        public static By Create_Event_Where_Location_Primary_Road_Name_DropDown => By.Id("primary_roadname_select_1");
        public static By Create_Event_Where_Location_Published_Road_Name => By.XPath("//label[contains(@class,'col-xs-5 control-label')][contains(text(),'Published Road Name')]");
        public static By Create_Event_Where_Location_Published_Road_Name_DropDown => By.Id("published_roadname_select_1");
        public static By Create_Event_Where_Location_Select_Primary_Road => By.Id("published_roadname_message_1");

        //To be revisited after Carly fixes it
        public static By Create_Event_Where_Location_Origin_Label => By.XPath("//div[contains(@class,'location-heading')][contains(text(),'Origin')]");
        public static By Create_Event_Where_Location_Origin_Address_Label => By.XPath("//span[contains(@class,'location-label')][contains(text(),'Address')]");
        public static By Create_Event_Where_Location_Origin_LGA_Label => By.XPath("//span[contains(@class,'location-label')][contains(text(),'LGA')]");
        public static By Create_Event_Where_Location_Origin_District_Label => By.XPath("//span[contains(@class,'location-label')][contains(text(),'District')]");
        public static By Create_Event_Where_Location_Origin_Road_ID_Label => By.XPath("//div[contains(@class,'col-xs-6')]//span[contains(@class,'location-label')][contains(text(),'Road Id')]");
        public static By Create_Event_Where_Location_Origin_TDist_Label => By.XPath("//div[contains(@class,'col-xs-6')]//span[contains(@class,'location-label')][contains(text(),'T-Dist')]");

        //To be revisited after Carly fixes it
        public static By Create_Event_Where_Location_Destination_Label => By.XPath("//div[contains(@class,'location-heading')][contains(text(),'Destination')]");

        //Xpath for destination will be added after carly fix
          //Will be added


        public static By Create_Event_Where_Location_Done_Button => By.Id("locationDoneButton");
        public static By Create_Event_Where_Error_Msg_PrimaryRoad_PublishedRoad_DropDown_Empty => By.XPath("//div[contains(@class,'alert alert-danger')]//div[contains(@class,'row')]");



        // *** WHY Section ***
        public static By Create_Event_Why_Header_text1 => By.XPath("//span[contains(@class,'panel-title')][contains(text(),'Why?')]");
        public static By Create_Event_Why_Header_EventCauseImpact_text2 => By.XPath("//span[contains(@class,'panel-title-description')][contains(text(),'Event Cause & Impact')]");
        public static By Create_Event_Why_Event_Description_Label => By.XPath("//label[contains(@for,'eventDescription')][contains(text(),'Event Description')]");
        public static By Create_Event_Why_Event_Description_TextBox => By.XPath("//textarea[contains(@class,'form-control resizable description')]");
        public static By Create_Event_Why_Event_Description_TextBox_Placeholder => By.XPath("//textarea[contains(@placeholder,'< add a short event description, to appear on web, phone and twitter. do not include event dates, impact of the event or any other duplicate information already entered >')]");
        public static By Create_Event_Why_Direction_Label => By.XPath("//label[contains(@for,'eventDirectionSelect')]");
        public static By Create_Event_Why_Direction_DropDown => By.XPath("//select[contains(@id,'eventDirectionSelect')]");
        public static By Create_Event_Why_Towards_Label => By.XPath("//label[contains(@for,'eventTowardsText')]");
        public static By Create_Event_Why_Towards_TextBox => By.XPath("//input[contains(@id,'eventTowardsText')]");
        public static By Create_Event_Why_Impact_Label => By.XPath("//label[contains(@for,'eventRoadImpactSelect')][contains(text(),'Impact')]");
        public static By Create_Event_Why_Impact_Dropdown => By.XPath("//select[contains(@id,'eventRoadImpactSelect')]");
        public static By Create_Event_Why_Impact_Details_Label => By.XPath("//label[contains(@for,'eventRoadImpactSelect')]");
        public static By Create_Event_Why_Impact_Details_DropDown => By.XPath("//select[contains(@id,'eventRoadImpactDetailsSelect')]");
        public static By Create_Event_Why_Delay_Label => By.XPath("//label[contains(@for,'eventDelaySelect')]");
        public static By Create_Event_Why_Delay_DropDown => By.XPath("//select[contains(@id,'eventDelaySelect')]");
        public static By Create_Event_Why_Advice_Label => By.XPath("//label[contains(@for,'eventAdviceSelect')]");
        public static By Create_Event_Why_Advice_Dropdown => By.XPath("//select[contains(@id,'eventAdviceSelect')]");
        public static By Create_Event_Why_Done_Button => By.XPath("//div[@class='col-xs-12 pull-right']//button[contains(@class,'btn btn-primary pull-right')][contains(text(),'Done')]");
        public static By Create_Event_Why_Static_text => By.XPath("//label[@class='small control-label'][contains(text(),'# Not externally published')]");
        public static By Create_Event_Why_LegalClosure_Label => By.XPath("//label[contains(@for,'eventYesLegalClosure')]");
        public static By Create_Event_Why_LegalClosure_Yes => By.XPath("//input[contains(@value,'Yes')]");
        public static By Create_Event_Why_LegalClosure_No => By.XPath("//input[contains(@value,'No')]");


        // *** WHEN Section ***
        public static By Create_Event_When_Header_Text1 => By.XPath("//span[contains(@class,'panel-title')][contains(text(),'When?')]");
        public static By Create_Event_When_Header_EventTiming_Text2 => By.XPath("//span[contains(@class,'panel-title-description')][contains(text(),'Event Timing')]");
        public static By Create_Event_When_Duration_Label => By.XPath("//span[contains(@class,'subheading')][contains(text(),'Duration')]");
        public static By Create_Event_When_Start_Date_Time_Label => By.XPath("//label[contains(@for,'eventDurationStartInput')][contains(text(),'Start Date & Time')]");
        public static By Create_Event_When_Calender_Textbox => By.XPath("//input[contains(@id,'eventDurationStartInput')]");
        public static By Create_Event_When_Calender_Icon => By.Id("duration-start-cal");
        public static By Create_Event_When_Active_Event_Static_text => By.XPath("//span[contains(@data-bind,'text: when.durationComments()')]");
        public static By Create_Event_When_Extra_Event_Info_Label => By.XPath("//label[contains(@for,'eventExtraInfo')]");
        public static By Create_Event_When_Extra_Event_Info_TextBox => By.Id("eventExtraInfo");
        public static By Create_Event_When_Area_Alerts_Label => By.XPath("//span[contains(@class,'subheading')][contains(text(),'Area Alerts')]");
        public static By Create_Event_When_Area_Alert_CheckBox => By.XPath("//label[contains(@for,'areaAlertCheckbox')]");
        public static By Create_Event_When_Area_Alert_CheckBox_ON => By.XPath("//span[contains(@class,'bootstrap-switch-handle-on bootstrap-switch-primary')][contains(text(),'ON')]");
        public static By Create_Event_When_Area_Alert_CheckBox_OFF => By.XPath("");
        public static By Create_Event_When_Alert_Label => By.XPath("//span[contains(@class,'subheading')][contains(text(),'Alerts')][2]");
        public static By Create_Event_When_Alert_Priority_label => By.XPath("//label[contains(@class,'col-xs-4 control-label')][contains(text(),'Priority')]");
        public static By Create_Event_When_Alert_Priority_Low => By.XPath("//div[contains(@class,'slider-tick round in-selection')][contains(@style,'left: 0%;')]");
        public static By Create_Event_When_Alert_Priority_Medium => By.XPath("//div[contains(@class,'slider-tick round')][contains(@style,'left: 33.3333%;')]");
        public static By Create_Event_When_Alert_Priority_High=> By.XPath("//div[contains(@class,'slider-tick round')][contains(@style,'left: 66.6667%;')]");
        public static By Create_Event_When_Alert_Priority_RedAlert => By.XPath("//div[contains(@class,'slider-tick round')][contains(@style,'left: 100%;')]");
        public static By Create_Event_When_Publication_Options_Label => By.XPath("//span[contains(@class,'subheading')][contains(text(),'Publication Options')]");
        public static By Create_Event_When_Publication_Options_Twitter => By.XPath("//label[contains(@for,'eventTwitterCheckbox')][contains(text(),'Twitter')]");
        public static By Create_Event_When_Publication_Options_Twitter_ON => By.XPath("");
        public static By Create_Event_When_Publication_Options_Twitter_OFF => By.XPath("");
        public static By Create_Event_When_Publication_Options_Phone => By.XPath("//label[contains(@for,'eventPhoneCheckbox')][contains(text(),'Phone')]");
        public static By Create_Event_When_Publication_Options_Phone_ON => By.XPath("");
        public static By Create_Event_When_Publication_Options_Phone_OFF => By.XPath("");

        public static By Create_Event_When_Internal_Label => By.XPath("//span[contains(@class,'subheading')][contains(text(),'Internal')]");
        public static By Create_Event_When_Next_Reminder_Label => By.XPath("//label[contains(@for,'nextReminderSelect')][contains(text(),'Next Reminder')]");
        public static By Create_Event_When_Next_Reminder_DropDown => By.XPath("//select[contains(@id,'nextReminderSelect')]");
        public static By Create_Event_When_Next_Reminder_Prompted_Date => By.XPath("//label[contains(@data-bind,'text: when.nextReminderDate')]");
        public static By Create_Event_When_Internal_Notes_Label => By.XPath("//label[contains(@for,'eventInternalNotesText')][contains(text(),'Internal Notes')]");
        public static By Create_Event_When_Internal_Notes_TextArea => By.XPath("//textarea[contains(@id,'eventInternalNotesText')]");
        public static By Create_Event_When_Done_Button => By.XPath("//div[@class='col - xs - 12']//button[@class='btn btn - primary pull - right'][contains(text(),'Done')][1]");
        public static By Create_Event_When_Not_Externally_Published_Text => By.XPath("//label[contains(@class,'small control-label')][contains(text(),'# Not externally published')]");







        //What now Section
        public static By Create_Event_WhatNow_Header_text1 => By.XPath("//span[contains(@class,'panel-title')][contains(text(),'What Now?')]");

        //Map Section
        public static By Create_Event_Map_Alter_Text => By.Id("mapAlertText");
        public static By Create_Event_OverMap_Attachment_Icon => By.Id("//div[@class='paperclip']//button[contains(@class,'btn btn-primary pull-right attachFilesButton')]//span[(@class='glyphicon glyphicon-paperclip')][1]");
    }
}