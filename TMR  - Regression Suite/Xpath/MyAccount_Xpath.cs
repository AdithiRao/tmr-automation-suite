﻿using OpenQA.Selenium;

namespace TMR_RegressionSuite.Xpath
{
    public static class MyAccount_Xpath
    {
        public static By Username_Dropdown => By.Id("dropdownMenu1");
        public static By Personal_Settings => By.XPath("//a[@href ='/Special/Account?page=personal']");
        public static By Change_Password => By.XPath("//a[@href ='/Special/Account?page=password']");

        //DropDown
        public static By DropDown_SignOut_Link => By.Id("p_lt_ctl00_SignOutButton_btnSignOutLink");
        public static By DropDown_Administration_Link => By.XPath("//a[@href='/Admin/']");
        public static By DropDown_Personalise_Link => By.Id("p_lt_ctl00_Link_Button1_btnElem_hyperLink");
        public static By DropDown_MyAccount_Link => By.Id("p_lt_ctl00_Link_Button_btnElem_lblText");


        //Personal Setting section
        public static By MyAccount_PersonalSetting_Username => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_UserName_lb");
        public static By MyAccount_PersonalSetting_User => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_UserName_label");
        public static By MyAccount_PersonalSetting_Fullname => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_FullName_lb");
        public static By MyAccount_PersonalSetting_Fullname_TextBox => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_FullName_txtText");
        public static By MyAccount_PersonalSetting_Email => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_Email_lb");
        public static By MyAccount_PersonalSetting_Email_TextBox => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_Email_txtEmailInput");
        public static By MyAccount_PersonalSetting_Save => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_btnOK");
        public static By MyAccount_PersonalSetting_InValidEmail_Error_Message => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_myProfile_editProfileForm_Email_lbe");


        //Change password section
        public static By MyAccount_ChangePassword_YourExisitingPassword => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_lblExistingPassword");
        public static By MyAccount_ChangePassword_YourExisitingPassword_Text => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_txtExistingPassword");
        public static By MyAccount_ChangePassword_NewPassword => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_lblPassword1");
        public static By MyAccount_ChangePassword_NewPassword_Text => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_passStrength_txtPassword");
        public static By MyAccount_ChangePassword_Password_Strength => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_passStrength_lblPasswStregth");
        public static By MyAccount_ChangePassword_ConfirmPassword => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_lblPassword2");
        public static By MyAccount_ChangePassword_ConfirmPassword_Text => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_txtPassword2");
        public static By MyAccount_ChangePassword_SetPassword_Button => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_btnOk");
        public static By MyAccount_ChangePassword_Not_Acceptable => By.Id("p_lt_ctl02_pageplaceholder_p_lt_ctl00_MyAccount_ucChangePassword_passStrength_lblEvaluation");


    }
}
