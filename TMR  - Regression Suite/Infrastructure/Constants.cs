﻿namespace TMR_RegressionSuite
{
    class Constants
    {
        public static string EPS_Homepage_URL = "https://eps.tst.qldtraffic.com.au/Special/Login.aspx?returnurl=%2f";
        public static string EPS_Homepage_Header_Sitename = "Department of Transport and Main Roads - Event Publishing System(EPS)";
        public static string Username = "AutomatedTestSuite";
        public static string Password = "a*1YnOK8!FFI";
        public static string InvalidPassword = "werwerwe";
        public static string InvalidEmail = "abc@";

        //CreateEvent page
        //EventType
        public static string CreateEvent_Hazard = "Hazard";
        public static string CreateEvent_Crash = "Crash";
        public static string CreateEvent_Congestion = "Congestion";
        public static string CreateEvent_Roadworks = "Roadworks";
        public static string CreateEvent_Special_Event = "Special event";
        public static string CreateEvent_Flooding = "Flooding";

        //Sub-Type
        public static string CreateEvent_Single_Vehicle = "Single vehicle";
        public static string CreateEvent_Multiple_Vehicle = "Multi-vehicle";

        public static string CreateEvent_Origin_Address = "2 Manin St, Wynnum QLD 4178, Australia";
        public static string CreateEvent_Destination_Address = "1 Berringar St, Wynnum QLD 4178, Australia";

        public static string CreateEvent_Valid_Date = "31 / 01 / 2020 1:29 PM";
    }
}
