﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;


namespace TMR_RegressionSuite.Infrastructure
{

    class BaseClass
    {
        static IWebDriver driver;

        public static IWebDriver GetDriver()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            return driver;
        }
        public void LoadApplication()
        {
            driver.Navigate().GoToUrl(Constants.EPS_Homepage_URL);
            Thread.Sleep(2000);
        }

        public void CloseBrowser()
        {
            driver.Close();
        }

        public void scrollToElement(String xpath)
        {
            var element = driver.FindElement(By.Id(xpath));
            Actions actions = new Actions(driver);
            actions.MoveToElement(element);
            actions.Perform();
        }

        public void waitUntil(By elementXpath)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementExists((elementXpath)));
        }

        

    }
}
   

    
