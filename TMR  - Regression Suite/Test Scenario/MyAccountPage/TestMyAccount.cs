﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using TMR_RegressionSuite.Infrastructure;
using TMR_RegressionSuite.Xpath;

namespace TMR_RegressionSuite.Test_Scenario.MyAccountPage
{
    class TestMyAccount : BaseClass
    {
        [TestCase]
        public void Verify_MyAccount_Landing_Page()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            //Validate the Personal Setting section
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.Personal_Settings).Displayed, "The Personal Setting Tab is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.Change_Password).Displayed, "The Change Password Tab is present");

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Username).Displayed, "The Username lable present");
            String UserNametext = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_User).Text;
            Assert.AreEqual(UserNametext, Constants.Username, "The username is displayed correctly");

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Fullname).Displayed, "The Full name lable is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Fullname_TextBox).Displayed, "The Full name is present");

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email).Displayed, "The Email lable is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox).Displayed, "The Email is present");

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Save).Displayed, "The Save button present");

            CloseBrowser();
        }

        [TestCase]
        public void Verify_Adding_Email_In_Personal_Setting_MyAccount()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            //Check Email text field is empty or not 
            IWebElement emailTextField = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox);
            String emailTextFieldValue = emailTextField.GetAttribute("value");
           
            if (!emailTextFieldValue.Equals(""))
            {
                emailTextField.Clear();
            }

            //Add Email in E-mail text box and save it
            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox).SendKeys("abc@gmail.com");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text in Email textbox" + ex.ToString());
                Assert.Fail("Unable add text in Email textbox");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Save).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Save button" + ex.ToString());
                Assert.Fail("Unable to click on Save button");
            }

            //Verify if the email is saved
            //Come back to Personal settings from Change password

            try
            {
                driver.FindElement(MyAccount_Xpath.Change_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Change Password" + ex.ToString());
                Assert.Fail("Unable to click on Change Password");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.Personal_Settings).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Personal Password" + ex.ToString());
                Assert.Fail("Unable to click on Personal Password");
            }

            //Extract the value from the email text
            IWebElement email = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox);
            String emailValue = email.GetAttribute("value");
            Assert.AreEqual("abc@gmail.com", emailValue, "The Email entered is saved correctly");

            CloseBrowser();
        }

        [TestCase]
        public void Verify_Adding_Invalid_Email_In_Personal_Setting_MyAccount()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            //Check Email text field is empty or not 
            IWebElement emailTextField = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox);
            String emailTextFieldValue = emailTextField.GetAttribute("value");

            if (!emailTextFieldValue.Equals(""))
            {
                emailTextField.Clear();
            }

            //Add Email in E-mail text box and save it
            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Email_TextBox).SendKeys(Constants.InvalidEmail);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text in Email textbox" + ex.ToString());
                Assert.Fail("Unable add text in Email textbox");
            }

            driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Save).Click();

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_InValidEmail_Error_Message).Displayed, "The Error message is displayed successfully");

            CloseBrowser();
        }

        [TestCase]
        public void Verify_Updating_FullName_In_Personal_Setting_MyAccount()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            //Clear Full name field if value exists 
            IWebElement fullName = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Fullname_TextBox);
            String fullNameText = fullName.GetAttribute("value");

            if (!fullNameText.Equals(""))
            {
                fullName.Clear();
            }

            //Update Full name and save it
            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Fullname_TextBox).SendKeys("NameUpdated");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable update the name in Full name text box" + ex.ToString());
                Assert.Fail("Unable update the name in Full name text box");
            }

            driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Save).Click();

            //Verify if the full name is saved
            //Come back to Personal settings from Change password

            try
            {
                driver.FindElement(MyAccount_Xpath.Change_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Change Password" + ex.ToString());
                Assert.Fail("Unable to click on Change Password");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.Personal_Settings).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Personal Password" + ex.ToString());
                Assert.Fail("Unable to click on Personal Password");
            }

            //Extract the value from the full name text box
            IWebElement fulName = driver.FindElement(MyAccount_Xpath.MyAccount_PersonalSetting_Fullname_TextBox);
            String fullNameValue = fulName.GetAttribute("Value");
            Assert.AreEqual("NameUpdated", fullNameValue, "The Fullname entered is saved correctly");

            CloseBrowser();
        }

        [TestCase]
        public void Verify_MyAccount_ChangePassword()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on:");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            //Validate the Personal Setting section
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.Personal_Settings).Displayed, "The Personal Setting Tab is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.Change_Password).Displayed, "The Change Password Tab is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.Change_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the change password" + ex.ToString());
                Assert.Fail("Unable to click on the change password");
            }

            //Verify the Change Password section

            //Your existing password
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_YourExisitingPassword).Displayed, "The Your existing password lable is present");

            IWebElement yourExistingPassword = driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_YourExisitingPassword_Text);
            Assert.IsTrue(yourExistingPassword.Displayed, "The Your existing password text box is present");
            String yourExistingPasswordValue = yourExistingPassword.GetAttribute("value");
            Assert.IsEmpty(yourExistingPasswordValue, "The Your existing password text box is empty");

            //New Password
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_NewPassword).Displayed, "The New password lable is present");

            IWebElement newPassword = driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_NewPassword_Text);
            Assert.IsTrue(newPassword.Displayed, "The New password text box is present");
            String newpasswordValue = newPassword.GetAttribute("value");
            Assert.IsEmpty(newpasswordValue, "The New password text box is empty");

            //Confirm Password
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_ConfirmPassword).Displayed, "The Confirm password lable is present");

            IWebElement confirmPassword = driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_ConfirmPassword_Text);
            Assert.IsTrue(confirmPassword.Displayed, "The Confirm password text box is present");
            String confirmPasswordValue = newPassword.GetAttribute("value");
            Assert.IsEmpty(confirmPasswordValue, "The confirm password text box is empty");

            //Set password
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_SetPassword_Button).Displayed, "The Set password button is present");

            CloseBrowser();
        }

        [TestCase]
        public void Verify_Updating_Password_In_Personal_Setting_MyAccount()
        {
            //Not executed - pending
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.Change_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Change password" + ex.ToString());
                Assert.Fail("Unable to click on Change password");
            }

            //Update Password and save it
            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_YourExisitingPassword_Text).SendKeys("sdfsdsdfsd");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text to Your existing password" + ex.ToString());
                Assert.Fail("Unable add text to Your existing password");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_NewPassword_Text).SendKeys("gerterter");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text to New password" + ex.ToString());
                Assert.Fail("Unable add text to New password");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_ConfirmPassword_Text).SendKeys("gerterter");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text to Confirm password" + ex.ToString());
                Assert.Fail("Unable add text to Confirm password");
            }

            driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_SetPassword_Button).Click();

            CloseBrowser();
        }

        [TestCase]
        public void Verify_Updating_Invalid_Password_In_Personal_Setting_MyAccount()
        {
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on:");
            }
            Thread.Sleep(14000);

            //Click on the Dropdown
            try
            {
                driver.FindElement(MyAccount_Xpath.Username_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the dropdown" + ex.ToString());
                Assert.Fail("Unable to click on the dropdown");
            }

            //Validate the dropdown
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_SignOut_Link).Displayed, "The Signout link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Administration_Link).Displayed, "The Administration link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_Personalise_Link).Displayed, "The Personalise link is present");
            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Displayed, "The My Account link is present");

            try
            {
                driver.FindElement(MyAccount_Xpath.DropDown_MyAccount_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Account link" + ex.ToString());
                Assert.Fail("Unable to click on the Account link");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.Change_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Change password" + ex.ToString());
                Assert.Fail("Unable to click on Change password");
            }

            //Update Password and save it
            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_YourExisitingPassword_Text).SendKeys("sdfsdsdfsd");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text to Your existing password" + ex.ToString());
                Assert.Fail("Unable add text to Your existing password");
            }

            try
            {
                driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_NewPassword_Text).SendKeys("try");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable add text to New password" + ex.ToString());
                Assert.Fail("Unable add text to New password");
            }

            Assert.IsTrue(driver.FindElement(MyAccount_Xpath.MyAccount_ChangePassword_Not_Acceptable).Displayed, "The Not acceptable text is present");


            CloseBrowser();
        }
    }
}