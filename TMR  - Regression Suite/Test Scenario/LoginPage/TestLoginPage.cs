﻿using OpenQA.Selenium;
using System.Threading;
using NUnit.Framework;
using TMR_RegressionSuite.Infrastructure;

namespace TMR_RegressionSuite.Test_Scenario.LoginPage
{
    class TestLoginPage : BaseClass
    {
        
        [TestCase]
        public void Verify_IF_User_Is_Able_To_Login()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_IF_User_Is_Able_To_Login testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(4000);
            //Validate the landing page
            
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");

            CloseBrowser();
            System.Diagnostics.Debug.WriteLine("Completed executing----Verify_IF_User_Is_Able_To_Login testcase");
        }

        [TestCase]
        public void Verify_Error_Message_When_User_Is_Unble_To_Login()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_Error_Message_When_User_Is_Unble_To_Login testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.InvalidPassword);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Element not found " + ex.ToString());
                Assert.Fail("Element not found");
            }

            //Validate the error message
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Login_Failure_Text).Displayed, "The error message is displayed successfully");

            CloseBrowser();
            System.Diagnostics.Debug.WriteLine("Completed executing----Verify_Error_Message_When_User_Is_Unble_To_Login testcase");
        }

        [TestCase]
        public void Verify_Forgotton_password()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_Forgotton_password testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.InvalidPassword);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on:" + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }

            //Validate the error message
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Login_Failure_Text).Displayed, "The error message is displayed successfully");

            //User clicks on Forgotten password link
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Forgotten_Password).Displayed, "The forgotten password link is displayed");

            try
            {
                driver.FindElement(Loginpage_Xpath.Forgotten_Password).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Forgotten password link" + ex.ToString());
                Assert.Fail("Unable to click on the Forgotten password link");
            }

            //Verify the reset password and email text box
            Thread.Sleep(2000);
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Homepage_Email).Displayed, "Email text box is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Reset_Password).Displayed, "Reset password buttom is present");

            CloseBrowser();
            System.Diagnostics.Debug.WriteLine("Completed executing----Verify_Forgotton_password testcase");
        }
    }
}
