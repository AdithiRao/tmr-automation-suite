﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using TMR_RegressionSuite.Infrastructure;
using TMR_RegressionSuite.Xpath;

namespace TMR_RegressionSuite.Test_Scenario.CreateEvent
{
    class CreateEvent : BaseClass
    {
        [TestCase]
        public void Verify_Create_Landing_Page()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_Create_Landing_Page testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);



            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on:");
            }
            Thread.Sleep(4000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");

            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch(NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");

            //What Section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_text1).Displayed, "What? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_EventType_text2).Displayed, "Event Type text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown_Label).Displayed, "Event Type label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Displayed, "Event Type dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown_Label).Displayed, "SubType label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Displayed, "SubType dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link).Displayed, "Web Link label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox).Displayed, "Web Link text box is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox_PlaceHolder).Displayed, "Web link placeholder is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Image).Displayed, "Image text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Attachment_Icon).Displayed, "Attachment Icon is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Displayed, "Done button is displayed succesfully");

            //Where section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            //Why Section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Header_text1).Displayed, "Why? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Cause & Impact text is displayed succesfully");

            //When Sectiom
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Header_Text1).Displayed, "When? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Header_EventTiming_Text2).Displayed, "Event Timing text is displayed succesfully");

            //What now Section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_WhatNow_Header_text1).Displayed, "What now? text is displayed succesfully");

            //Map Section
            IWebElement mapALtertext = driver.FindElement(CreateEvent_Xpath.Create_Event_Map_Alter_Text);
            Assert.AreEqual("Fill in the What panel to activate the map.", mapALtertext.Text, "Map Alt text - Fill in the What panel to activate the map. is displayed succesfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_OverMap_Attachment_Icon).Displayed, "Attachemnt paperclip icon is displayed succesfully over the map");

            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_Create_Landing_Page testcase");
        }

        [TestCase]
        public void Verify_If_User_Is_Able_Fill_Detail_What_Section_Create_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_If_User_Is_Able_Fill_Detail_What_Section_Create_Event testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");

            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");


            //Verify the What section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_text1).Displayed, "What? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_EventType_text2).Displayed, "Event Type text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown_Label).Displayed, "Event Type label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Displayed, "Event Type dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown_Label).Displayed, "SubType label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Displayed, "SubType dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link).Displayed, "Web Link label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox).Displayed, "Web Link text box is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox_PlaceHolder).Displayed, "Web link placeholder is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Image).Displayed, "Image text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Attachment_Icon).Displayed, "Attachment Icon is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Displayed, "Done button is displayed succesfully");


            //Enter the details in What section

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            //Verify the Where section upon clickin Done
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box).Displayed, "Origin text box is displayed succesfully after clicking Done button");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Placeholder_Text).Displayed, "Origin placeholder text - Origin is displayed succesfully");

            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_If_User_Is_Able_Fill_Detail_What_Section_Create_Event testcase");
        }

        [TestCase]
        public void Verify_If_User_Is_Able_Fill_Detail_Where_Section_Create_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_If_User_Is_Able_Fill_Detail_Where_Section_Create_Event test case");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");


            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            //Verify the Where section upon clickin Done
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box).Displayed, "Origin text box is displayed succesfully after clicking Done button");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Placeholder_Text).Displayed, "Origin placeholder text - Origin is displayed succesfully");


            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box).Displayed, "Destination text box is displayed succesfully after entering Origin address");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Placeholder_Text).Displayed, "Destination text box placeholder text - Destination is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Displayed, "Add Location button is displayed successfully after entering Origin button");

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Verify the loaction section 
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Number_Heading).Displayed, "Location: X is displayed successfully");              //Note X is used here. Loop to be implemented and the number to be retrieved
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Remove_Location).Displayed, "Remove Location button is displayed successfully");  
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Edit_Location).Displayed, "Edit location button is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_Label).Displayed, " Primary Road Name Label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown).Displayed, "Primary Road Name Drop down is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Published_Road_Name).Displayed, " Published Road Name is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_Label).Displayed, "Origin label in Location section is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_Address_Label).Displayed, "Origin- Address label in Location section is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_LGA_Label).Displayed, "Origin- GA label in Location section is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_District_Label).Displayed, "Origin-District label in Location section is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_Road_ID_Label).Displayed, "Origin-Road Id label in Location section is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Origin_TDist_Label).Displayed, "Origin-T-Dist label in Location section is displayed successfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Destination_Label).Displayed, " Destination label in Location section is displayed successfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Displayed, " Done button in Location section is displayed successfully");


            //Select an option from the Primary Road Name option
            try
            {
                SelectElement primaryRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown));
                primaryRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Primay road from the PrimaryRoad Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Primay road from the PrimaryRoad Name Dropdown ");
            }

            Thread.Sleep(2000);
            //Select an option from the Published Road Name option
            try
            {
                SelectElement publishedRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Published_Road_Name_DropDown));
                publishedRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Published road from the Published Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Published road from the Published Road Name Dropdown ");
            }

            //Click on the Done button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_If_User_Is_Able_Fill_Detail_Where_Section_Create_Event testcase");

        }

        [TestCase]
        public void Verify_Create_Event_ErrorMsg_PrimaryRoad_Empty()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_Create_Event_ErrorMsg_PrimaryRoad_Empty testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");


            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            //Verify the Where section upon clicking Done
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box).Displayed, "Origin text box is displayed succesfully after clicking Done button");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Placeholder_Text).Displayed, "Origin placeholder text - Origin is displayed succesfully");


            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Click on the Done button without selecting primary road
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            //Error message should be displayed
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Error_Msg_PrimaryRoad_PublishedRoad_DropDown_Empty).Displayed, "The error message - is displayed successfully");

           CloseBrowser();

           System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_Create_Event_ErrorMsg_PrimaryRoad_Empty testcase");

        }

        [TestCase]
        public void Verify_Create_Event_ErrorMsg_PublishedRoad_Empty()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_Create_Event_ErrorMsg_PublishedRoad_Empty testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");


            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            //Verify the Where section upon clicking Done
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box).Displayed, "Origin text box is displayed succesfully after clicking Done button");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Placeholder_Text).Displayed, "Origin placeholder text - Origin is displayed succesfully");


            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Select an option from the Primary Road Name option
            try
            {
                SelectElement primaryRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown));
                primaryRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Primay road from the PrimaryRoad Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Primay road from the PrimaryRoad Name Dropdown ");
            }

            Thread.Sleep(2000);

            //Click on the Done button without selecting published road
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            //Error message should be displayed
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Error_Msg_PrimaryRoad_PublishedRoad_DropDown_Empty).Displayed, "The error message - is displayed successfully");

            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_Create_Event_ErrorMsg_PublishedRoad_Empty testcase");

        }

        [TestCase]
        public void Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event test case");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");

            // *** WHAT Section ***
            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Select an option from the Primary Road Name option
            try
            {
                SelectElement primaryRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown));
                primaryRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Primay road from the PrimaryRoad Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Primay road from the PrimaryRoad Name Dropdown ");
            }

            Thread.Sleep(2000);
            //Select an option from the Published Road Name option
            try
            {
                SelectElement publishedRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Published_Road_Name_DropDown));
                publishedRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Published road from the Published Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Published road from the Published Road Name Dropdown ");
            }

            //Click on the Done button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            // ***** WHY Section *****

            //Verify the Why section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Header_text1).Displayed, "The Why text is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Header_EventCauseImpact_text2).Displayed, "The Event Cause Impact is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_Label).Displayed, "The Event Description Label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_TextBox).Displayed, "The Event Description TextBox is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_TextBox_Placeholder).Displayed, "The Event Description TextBox Placeholder is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Direction_Label).Displayed, "The Direction label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Direction_DropDown).Displayed, "The Direction dropdown is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_Label).Displayed, "The Towards label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).Displayed, "The Towards dropdown is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Label).Displayed, "The Impact label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown).Displayed, "The Impact dropdown is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_Label).Displayed, "The Impact Details label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown).Displayed, "The Impact Details dropdown is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Delay_Label).Displayed, "The Delay label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Delay_DropDown).Displayed, "The Delay dropdown is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Advice_Label).Displayed, "The Advice label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Advice_Dropdown).Displayed, "The  is displayed successfully");

            //Enter the details in Event Description
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_TextBox).SendKeys("The event description text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add the text in Event description text box" + ex.ToString());
                Assert.Fail("Unable to add the text in Event description text box");
            }

            //Choose an option from Direction dropdown
            try
            {
                SelectElement directionDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Direction_DropDown));
                directionDropDown.SelectByIndex(4);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Direction Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Direction Dropdown");
            }

            //Verify if Towards, Impact are enabled
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).Enabled, "The Towards dropdown is enabled successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown).Enabled, "The Impact dropdown is enabled successfully");
           
            //Choose an option from Towards textbox
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).SendKeys("The Towards text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add text to Towards text box" + ex.ToString());
                Assert.Fail("Unable to add text to Towards text box");
            }

            //Choose an option from Impact dropdown
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown));
                impactDropDown.SelectByIndex(3);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Impact Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Impact Dropdown");
            }

            Thread.Sleep(2000);

            //Verify if Impact Details, Delay are enabled
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown).Enabled, "The Impact Details dropdown is enabled successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Delay_DropDown).Enabled, "The Delay dropdown is enabled successfully");

            //Choose an option from ImpactDetails dropdown
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown));
                impactDropDown.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from ImpactDetails Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from ImpactDetails Dropdown");
            }

            //Choose an option from Delay dropdown
            try
            {
                SelectElement impactDetailsDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Delay_DropDown));
                impactDetailsDropDown.SelectByIndex(1);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Delay Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Delay Dropdown");
            }

            //Choose an option from Advice dropdown
            try
            {
                SelectElement adviceDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Advice_Dropdown));
                adviceDropDown.SelectByIndex(1);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Delay Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Delay Dropdown");
            }

            //Clicking on Done Button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Done button" + ex.ToString());
                Assert.Fail("Unable to click on Done button");
            }


            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event testcase");

        }

        [TestCase]
        public void Verify_LegalAdvise_Display_Hidden_While_Create_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event test case");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");

            // *** WHAT Section ***
            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Select an option from the Primary Road Name option
            try
            {
                SelectElement primaryRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown));
                primaryRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Primay road from the PrimaryRoad Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Primay road from the PrimaryRoad Name Dropdown ");
            }

            Thread.Sleep(2000);
            //Select an option from the Published Road Name option
            try
            {
                SelectElement publishedRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Published_Road_Name_DropDown));
                publishedRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Published road from the Published Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Published road from the Published Road Name Dropdown ");
            }

            //Click on the Done button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            // ***** WHY Section *****

            //Enter the details in Event Description
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_TextBox).SendKeys("The event description text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add the text in Event description text box" + ex.ToString());
                Assert.Fail("Unable to add the text in Event description text box");
            }

            //Choose an option from Direction dropdown
            try
            {
                SelectElement directionDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Direction_DropDown));
                directionDropDown.SelectByIndex(4);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Direction Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Direction Dropdown");
            }

            //Verify if Towards, Impact are enabled
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).Enabled, "The Towards dropdown is enabled successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown).Enabled, "The Impact dropdown is enabled successfully");

            //Choose an option from Towards textbox
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).SendKeys("The Towards text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add text to Towards text box" + ex.ToString());
                Assert.Fail("Unable to add text to Towards text box");
            }

            //Choose N/A option from Impact dropdown
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown));
                impactDropDown.SelectByIndex(1);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Impact Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Impact Dropdown");
            }

            Thread.Sleep(2000);

            // Verify ImpactDetails dropdown is disabled when N/A option is chosen
            Assert.False(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown).Enabled, "The Impact Details dropdown is not enabled");

            //**** Legal clsoure Displayed*****
            //Choose Closure option from Impact dropdown 
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown));
                impactDropDown.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Impact Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Impact Dropdown");
            }

            Thread.Sleep(2000);

            // Verify ImpactDetails dropdown is enabled and Legal Closure option is displayed when Closure option is chosen
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown).Enabled, "The Impact Details dropdown is enabled");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_Label).Displayed, "The Legal Closure label is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_Yes).Displayed, "The Legal Closure - Yes radio button is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_No).Displayed, "The Legal Closure - No radio button is displayed");

            //**** Legal clsoure Hidden*****
            //Choose any options Lane affected /Lane blocked/No blockage from Impact dropdown 
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown));
                impactDropDown.SelectByIndex(4);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Impact Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Impact Dropdown");
            }

            Thread.Sleep(2000);
            // Verify ImpactDetails dropdown is enabled and Legal Closure option is hidden when Lane affected /Lane blocked/No blockage option is chosen
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown).Enabled, "The Impact Details dropdown is enabled");
            Assert.False (driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_Label).Displayed, "The Legal Closure label is not displayed");
            Assert.False(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_Yes).Displayed, "The Legal Closure - Yes radio button is not displayed");
            Assert.False(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_LegalClosure_No).Displayed, "The Legal Closure - No radio button is not displayed");

            CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event testcase");

        }

        [TestCase]
        public void Verify_If_User_Is_Able_Fill_Detail_When_Section_Create_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing----Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event test case");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");


            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");

            // *** WHAT Section ***
            //Enter the details in What section
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

            //Click on the Add Location button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Add Location button" + ex.ToString());
                Assert.Fail("Unable to click on the Add Location button");
            }

            //Wait until all the location section is loaded
            waitUntil(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button);
            Thread.Sleep(2000);

            //Select an option from the Primary Road Name option
            try
            {
                SelectElement primaryRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Primary_Road_Name_DropDown));
                primaryRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Primay road from the PrimaryRoad Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Primay road from the PrimaryRoad Name Dropdown ");
            }

            Thread.Sleep(2000);
            //Select an option from the Published Road Name option
            try
            {
                SelectElement publishedRoadNames = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Published_Road_Name_DropDown));
                publishedRoadNames.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Published road from the Published Name Dropdown " + ex.ToString());
                Assert.Fail("Unable select Published road from the Published Road Name Dropdown ");
            }

            //Click on the Done button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Location_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable click Done button from the Location section " + ex.ToString());
                Assert.Fail("Unable click Done button from the Location section ");
            }

            // ***** WHY Section *****

            //Enter the details in Event Description
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Event_Description_TextBox).SendKeys("The event description text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add the text in Event description text box" + ex.ToString());
                Assert.Fail("Unable to add the text in Event description text box");
            }

            //Choose an option from Direction dropdown
            try
            {
                SelectElement directionDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Direction_DropDown));
                directionDropDown.SelectByIndex(4);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Direction Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Direction Dropdown");
            }

            //Choose an option from Towards textbox
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Towards_TextBox).SendKeys("The Towards text box details added");
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add text to Towards text box" + ex.ToString());
                Assert.Fail("Unable to add text to Towards text box");
            }

            //Choose an option from Impact dropdown
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Dropdown));
                impactDropDown.SelectByIndex(3);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Impact Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Impact Dropdown");
            }

            Thread.Sleep(2000);

            //Choose an option from ImpactDetails dropdown
            try
            {
                SelectElement impactDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Impact_Details_DropDown));
                impactDropDown.SelectByIndex(2);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from ImpactDetails Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from ImpactDetails Dropdown");
            }

            //Choose an option from Delay dropdown
            try
            {
                SelectElement impactDetailsDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Delay_DropDown));
                impactDetailsDropDown.SelectByIndex(1);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Delay Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Delay Dropdown");
            }

            //Choose an option from Advice dropdown
            try
            {
                SelectElement adviceDropDown = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Advice_Dropdown));
                adviceDropDown.SelectByIndex(1);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to select an option from Delay Dropdown" + ex.ToString());
                Assert.Fail("Unable to select an option from Delay Dropdown");
            }

            //Clicking on Done Button
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_Why_Done_Button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on Done button" + ex.ToString());
                Assert.Fail("Unable to click on Done button");
            }

            Thread.Sleep(2000);

            // ***** WHEN Section *****
            //Validate the When section

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Header_Text1).Displayed, "The When header text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Header_EventTiming_Text2).Displayed, "The Event Timings sub header text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Duration_Label).Displayed, "The Duration label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Start_Date_Time_Label).Displayed, "The Start Date Time label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Calender_Textbox).Displayed, "The calender text box is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Calender_Icon).Displayed, "The calender icon is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Active_Event_Static_text).Displayed, "The text - 'The event will be active from ... ' is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Extra_Event_Info_Label).Displayed, "The Extra Event Info label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Extra_Event_Info_TextBox).Displayed, "The Extra Event Info text box is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Area_Alerts_Label).Displayed, "The Area Alert Sub header label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Area_Alert_CheckBox).Displayed, "The Area Alert check box label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Area_Alert_CheckBox_ON).Displayed, "The Area Alert check box option ON  displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Area_Alert_CheckBox_OFF).Displayed, "The Area Alert check box option OFF displayed");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Label).Displayed, "The Alert label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_label).Displayed, "The Alert - Priority label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_Low).Displayed, "The Alert - Priority Low label is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_Medium).Displayed, "The Alert - Priority Medium label is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_High).Displayed, "The Alert - Priority High label is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_RedAlert).Displayed, "The Alert - Priority Red Alert label is displayed");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Alert_Priority_Low).Displayed, "The Alert - Priority Low label is displayed");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Label).Displayed, "The Publication Option label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Twitter).Displayed, "The Publication Option _ Twitter label is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Twitter_ON).Displayed, "The Twitter - ON is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Twitter_OFF).Displayed, "The  Twitter - OFF is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Phone).Displayed, "The Publication Option _ Phone label is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Phone_ON).Displayed, "The Phone - ON is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Publication_Options_Phone_OFF).Displayed, "The  Phone - OFF is displayed successfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Internal_Label).Displayed, "The Internal label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Next_Reminder_Label).Displayed, "The Next Reminder label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Next_Reminder_DropDown).Displayed, "The Next Reminder dropdown label is displayed successfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Next_Reminder_Prompted_Date).Displayed, "The date prompt is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Internal_Notes_Label).Displayed, "The Internal Notes label is displayed successfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Internal_Notes_TextArea).Displayed, "The Internal Notes text box is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Done_Button).Displayed, "The Done button is displayed successfully");
            //Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_When_Not_Externally_Published_Text).Displayed, "The Not externally published static text is displayed successfully");


            // Add details in WHEN section

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_When_Calender_Textbox).SendKeys(Constants.CreateEvent_Valid_Date);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add text to Calender textbox " + ex.ToString());
                Assert.Fail("Unable to add text to Calender textbox ");
            }

           






            //CloseBrowser();

            System.Diagnostics.Debug.WriteLine("Execution Completed---Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event testcase");

        }

        [TestCase]
        public void Verify_If_User_Is_Able_To_Create_Crash_Event()
        {
            System.Diagnostics.Debug.WriteLine("Executing---Verify_If_User_Is_Able_To_Create_Crash_Event testcase");
            IWebDriver driver = GetDriver();
            LoadApplication();
            Thread.Sleep(2000);

            //Enter username , password and click on Sign on
            try
            {
                driver.FindElement(Loginpage_Xpath.Username).SendKeys(Constants.Username);
                driver.FindElement(Loginpage_Xpath.Password).SendKeys(Constants.Password);
                driver.FindElement(Loginpage_Xpath.Sign_In).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Error occurred logging on: " + ex.ToString());
                Assert.Fail("Error occurred logging on: ");
            }
            Thread.Sleep(14000);

            //Validate the landing page
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Logo).Displayed, "The home page logo is present");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Site_name).Displayed, "The header site name is present");
            Assert.AreEqual(driver.FindElement(Loginpage_Xpath.Header_Site_name).Text, "Department of Transport and Main Roads - Event Publishing System (EPS)");
            Assert.IsTrue(driver.FindElement(Loginpage_Xpath.Header_Text).Displayed, "The header text is present");

            //Click on Create Link
            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Link).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click on the Create link" + ex.ToString());
                Assert.Fail("Unable to click on the Create link");
            }

            //Verify the Create Landing Page
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Link).Displayed, "Create link is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Tab).Displayed, "Create Event Tab is displayed succesfully");


            //Verify the What section
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_text1).Displayed, "What? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Header_EventType_text2).Displayed, "Event Type text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown_Label).Displayed, "Event Type label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Displayed, "Event Type dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown_Label).Displayed, "SubType label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Displayed, "SubType dropdown is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link).Displayed, "Web Link label is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox).Displayed, "Web Link text box is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Web_Link_Textbox_PlaceHolder).Displayed, "Web link placeholder is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Image).Displayed, "Image text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Attachment_Icon).Displayed, "Attachment Icon is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Displayed, "Done button is displayed succesfully");


            //Enter the details in What section

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click EventType Dropdown" + ex.ToString());
                Assert.Fail("Unable to click EventType Dropdown");
            }
            try
            {
                SelectElement eventTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_EventType_Dropdown));
                eventTypes.SelectByValue(Constants.CreateEvent_Crash);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Crash option from the Event Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Crash option from the Event Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Sub-Type Dropdown" + ex.ToString());
                Assert.Fail("Unable to click Sub-Type Dropdown");
            }
            try
            {
                SelectElement subTypes = new SelectElement(driver.FindElement(CreateEvent_Xpath.Create_Event_What_SubType_Dropdown));
                subTypes.SelectByText(Constants.CreateEvent_Multiple_Vehicle);
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable select Single option from the Sub Type Dropdown" + ex.ToString());
                Assert.Fail("Unable select Single option from the Sub Type Dropdown");
            }

            try
            {
                driver.FindElement(CreateEvent_Xpath.Create_Event_What_Done_button).Click();
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to click Done button in What section" + ex.ToString());
                Assert.Fail("Unable to click Done button in What section");
            }
            Thread.Sleep(2000);

            //Verify the Where section upon clickin Done
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_text1).Displayed, "Where? text is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Header_EventLocation_text2).Displayed, "Event Location text is displayed succesfully");

            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box).Displayed, "Origin text box is displayed succesfully after clicking Done button");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Placeholder_Text).Displayed, "Origin placeholder text - Origin is displayed succesfully");

            //Enter the details in where section

            try
            {
                IWebElement origin_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Origin_Text_Box);
                origin_Address.SendKeys(Constants.CreateEvent_Origin_Address);

                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter); 

                    Thread.Sleep(2000);
                    System.Diagnostics.Debug.WriteLine("11111111");
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click on the google suggestion");
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Origin textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Origin textbox");
            }
            Thread.Sleep(2000);
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box).Displayed, "Destination text box is displayed succesfully after entering Origin address");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Placeholder_Text).Displayed, "Destination text box placeholder text - Destination is displayed succesfully");
            Assert.True(driver.FindElement(CreateEvent_Xpath.Create_Event_Where_AddLocation_Button).Displayed, "Add Location button is displayed successfully after entering Origin button");

            try
            {
                IWebElement destination_Address = driver.FindElement(CreateEvent_Xpath.Create_Event_Where_Destination_Text_Box);
                destination_Address.SendKeys(Constants.CreateEvent_Destination_Address);
                try
                {
                    Thread.Sleep(2000);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.ArrowDown);
                    driver.SwitchTo().ActiveElement().SendKeys(Keys.Enter);

                    Thread.Sleep(2000);
                }

                catch (NoSuchElementException ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"Unable to click  on the google suggestion" + ex.ToString());
                    Assert.Fail("Unable to click  on the google suggestion");
                }

            }
            catch (NoSuchElementException ex)
            {
                System.Diagnostics.Debug.WriteLine(@"Unable to add address in Destination textbox" + ex.ToString());
                Assert.Fail("Unable to add address in Destination textbox");
            }

            //Verify the Origin and Destination Panel
            //Pending - Code changes awaited from Carly

           











            //CloseBrowser();

        }
        }
    }
