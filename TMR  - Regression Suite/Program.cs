﻿
using TMR_RegressionSuite.Test_Scenario.CreateEvent;
using TMR_RegressionSuite.Test_Scenario.LoginPage;
using TMR_RegressionSuite.Test_Scenario.MyAccountPage;

namespace TMR____Regression_Suite
{
    class Program
    {
        public static void Main(string[] args)
        {
            TestLoginPage loginpage = new TestLoginPage();
            //loginpage.Verify_IF_User_Is_Able_To_Login();
            //loginpage.Verify_Error_Message_When_User_Is_Unble_To_Login();
            //loginpage.Verify_Forgotton_password();

            TestMyAccount accountpage = new TestMyAccount();
            //accountpage.Verify_MyAccount_Landing_Page();
            //accountpage.Verify_Adding_Email_In_Personal_Setting_MyAccount();
            //accountpage.Verify_Adding_Invalid_Email_In_Personal_Setting_MyAccount();
            //accountpage.Verify_Updating_FullName_In_Personal_Setting_MyAccount();
            //accountpage.Verify_MyAccount_ChangePassword(); -- do not execute now
            //accountpage.Verify_Updating_Password_In_Personal_Setting_MyAccount();
            //accountpage.Verify_Updating_Invalid_Password_In_Personal_Setting_MyAccount();

            CreateEvent createAccount = new CreateEvent();
            //createAccount.Verify_Create_Landing_Page();
            //createAccount.Verify_If_User_Is_Able_Fill_Detail_What_Section_Create_Event();
            //createAccount.Verify_If_User_Is_Able_Fill_Detail_Where_Section_Create_Event();
            //createAccount.Verify_Create_Event_ErrorMsg_PrimaryRoad_Empty();
            //createAccount.Verify_Create_Event_ErrorMsg_PublishedRoad_Empty();
            // createAccount.Verify_If_User_Is_Able_Fill_Detail_Why_Section_Create_Event();
            //createAccount.Verify_LegalAdvise_Display_Hidden_While_Create_Event();
            createAccount.Verify_If_User_Is_Able_Fill_Detail_When_Section_Create_Event();
            //createAccount.Verify_If_User_Is_Able_To_Create_Crash_Event();

           
        }
    }
}
